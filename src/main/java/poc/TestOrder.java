/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poc;

import com.werapan.databaseproject.dao.OrdersDao;
import com.werapan.databaseproject.model.OrderDetail;
import com.werapan.databaseproject.model.Orders;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author User
 */
public class TestOrder {

    public static void main(String[] args) {
        Product product1 = new Product(1, "A", 75);
        Product product2 = new Product(2, "B", 180);
        Product product3 = new Product(3, "C", 70);
        Orders order = new Orders();
//        OrderDetial orderdetial1 = new OrderDetial(product1,product1.getName(),product1.getPrice(),1,order);
//        order.addOrderDetial(orderdetial1);
        order.addOrderDetial(product1, 10);
        order.addOrderDetial(product2, 5);
        order.addOrderDetial(product3, 3);
        System.out.println(order);
        System.out.println(order.getOrderDetails());
        printReciept(order);

        OrdersDao orderDao = new OrdersDao();
//        orderDao.save(order);
        Orders neworder = orderDao.save(order);
       System.out.println(neworder);

//        System.out.println(orderDao.get(3));

    }

    static void printReciept(Orders order) {
        System.out.println("Order " + order.getId());
        for (OrderDetail od : order.getOrderDetails()) {
            System.out.println(" " + od.getProductName() + " " + od.getQty()
                    + " " + od.getProductPrice() + " " + od.getTotal());
        }
        System.out.println("Total: " + order.getTotal() + "Qty: " + order.getQty());
    }

}
