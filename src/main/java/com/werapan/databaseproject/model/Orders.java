/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Orders {

    private int id;
    private Date date;
    private int qty;
    private double total;
    private ArrayList<OrderDetail> orderDetials;

    public Orders(int id, Date date, int qty, double total, ArrayList<OrderDetail> orderDetials) {
        this.id = id;
        this.date = date;
        this.qty = qty;
        this.total = total;
        this.orderDetials = orderDetials;
    }

    public Orders() {
        this.id = -1;
        orderDetials = new ArrayList<>();
        qty=0;
        total = 0;
        date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<OrderDetail> getOrderDetails() {
        return orderDetials;
    }

    public void setOrderDetials(ArrayList<OrderDetail> orderDetials) {
        this.orderDetials = orderDetials;
    }

    @Override
    public String toString() {
        return "Orders{" + "id=" + id + ", date=" + date + ", qty=" + qty + ", total=" + total +  '}';
    }
    
    public void addOrderDetial(OrderDetail orderDetial){
        orderDetials.add(orderDetial);
        total = total + orderDetial.getTotal();
        qty = qty+orderDetial.getQty();
    }
     public void addOrderDetial(Product product,String productName, double productPrice,int qty){
        OrderDetail orderDetail = new OrderDetail(product,productName,productPrice,qty,this);
        this.addOrderDetial(orderDetail);
     }
      public void addOrderDetial(Product product,int qty){
        OrderDetail orderDetail = new OrderDetail(product,product.getName(),product.getPrice(),qty,this);
        this.addOrderDetial(orderDetail);
     }
         public static Orders fromRS(ResultSet rs) {
        Orders order = new Orders();
        try {
            order.setId(rs.getInt("orders_id"));
            order.setQty(rs.getInt("order_qty"));
            order.setTotal(rs.getDouble("order_total"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            order.setDate(sdf.parse(rs.getString("order_date")));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }

}
